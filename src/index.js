const express = require('express')
require('./db/mongoose')
const cors = require('cors')
const app = express()
const port = 3000

const userRouter = require('./routers/user')
const categoryRouter = require('./routers/category')
const groupRouter = require('./routers/group')
const bookRouter = require('./routers/book')

app.use(express.json())
app.use(cors())
app.use(userRouter)
app.use(categoryRouter)
app.use(groupRouter)
app.use(bookRouter)

app.listen(port, () => {
    console.log(`Server is running in ${port}`)
})