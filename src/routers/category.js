const express = require('express')
const Category = require('../models/category')
const auth = require('../middleware/auth')
const router = new express.Router()

//create
router.post('/category', auth, async (req, res) => {
    const checkname = await Category.findOne(
        {"category_name": req.body.category_name}
    )
    if (checkname) {
        return res
            .status(200)
            .send({status: false, message: 'sorry! category name is already', data: ''})
    }

    const category = new Category()
    category.category_name = req.body.category_name
    category.owner = req.body.owner
    try {
        await category.save()
        res
            .status(201)
            .send({status: true, message: 'success save data.', data: category})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//read
router.get('/category', auth, async (req, res) => {
    try {
        const category = await Category
            .find({})
            .populate('owner')
        res
            .status(200)
            .send({status: true, message: 'success read data.', data: category})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//readEdit
router.get('/category/:id', auth, async (req, res) => {
    try {
        const category = await Category
            .findById(req.params.id)
            .populate('owner')
        res
            .status(201)
            .send({status: true, message: 'success read data.', data: category})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//update
router.patch('/category', auth, async (req, res) => {  
    try {
        const category = await Category.findByIdAndUpdate(req.body.id, {
            category_name: req.body.category_name
        })
        res
            .status(201)
            .send({status: true, message: 'success update data.', data: category})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//delete
router.delete('/category/:id', auth, async (req, res) => {
    try {
        const category = await Category.findByIdAndDelete(req.params.id)
        if (!category) {
            return res
            .status(200)
            .send({status: false, message: 'failed delete data not found!', data: ''})
        }
        res
            .status(200)
            .send({status: true, message: 'success delete data.', data: category})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

module.exports = router