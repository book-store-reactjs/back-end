const express = require('express')
const User = require('../models/user')
const router = new express.Router()
const bcrypt = require('bcryptjs')
const auth = require('../middleware/auth')

router.post('/users/register', async (req, res) => {
    const checkemail = await User.findOne({"email": req.body.email})
    if (checkemail) {
       return res
        .status(200)
        .send({status: false, message: 'sorry! failed register email is already', data: ''})
    }

    const user = new User() 
    user.name = req.body.name
    user.email = req.body.email
    user.password = req.body.password
    user.group = 'User'
    try {
        await user.save()
        res
            .status(201)
            .send({status: true, message: 'success register user, please wait redirect to login', data: user})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: 'sorry! failed register user please chek your input', data: e})
    }
})

router.post('/users/login', async (req, res) => {
    try {
        const user = await User.findOne({"email": req.body.email})
        if (user) {
            const isMatch = await bcrypt.compare(req.body.password, user.password)
            if (isMatch) {
                var token = await user.generateAuthToken()
                res
                    .status(200)
                    .send({status: true, message: 'success login user please wait redirect to home', data: user, token: token})
            } else {
                res
                    .status(200)
                    .send({status: false, message: 'password is wrong!', data: []})
            }
        } else {
            res
                .status(200)
                .send({status: false, message: 'email not found!', data: []})
        }
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

router.post('/users/logout', auth, async (req, res) => {
    try {
      req.user.tokens = req.user.tokens.filter(token => {
        return token.token !== req.token
      })
      await req.user.save()
  
      res.send({ message: 'Logout Success' })
    } catch (e) {
      res.status(500).send({ message: 'Logout Gagal' })
    }
})

//create manual
router.post('/user', auth, async (req, res) => {
    const checkname = await User.findOne(
        {"user_name": req.body.user_name}
    )
    if (checkname) {
        return res
            .status(200)
            .send({status: false, message: 'sorry! user name is already', data: ''})
    }

    const user = new User()
    user.user_name = req.body.user_name
    user.owner = req.body.owner
    try {
        await user.save()
        res
            .status(201)
            .send({status: true, message: 'success save data.', data: user})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//read
router.get('/user', auth, async (req, res) => {
    try {
        const user = await User
            .find({})
            .populate('owner')
        res
            .status(200)
            .send({status: true, message: 'success read data.', data: user})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//readEdit
router.get('/user/:id', auth, async (req, res) => {
    try {
        const user = await User
            .findById(req.params.id)
            .populate('owner')
        res
            .status(201)
            .send({status: true, message: 'success read data.', data: user})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//update
router.patch('/user', auth, async (req, res) => {  
    try {
        const user = await User.findByIdAndUpdate(req.body.id, {
            user_name: req.body.user_name
        })
        res
            .status(201)
            .send({status: true, message: 'success update data.', data: user})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//delete
router.delete('/user/:id', auth, async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id)
        if (!user) {
            return res
            .status(200)
            .send({status: false, message: 'failed delete data not found!', data: ''})
        }
        res
            .status(200)
            .send({status: true, message: 'success delete data.', data: user})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

module.exports = router