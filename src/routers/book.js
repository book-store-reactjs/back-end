const express = require('express')
const Book = require('../models/book')
const auth = require('../middleware/auth')
const router = new express.Router()

//create
router.post('/book', auth, async (req, res) => {
    const book = new Book()
    book.title = req.body.title
    book.description = req.body.description
    book.category = req.body.category
    book.stock = req.body.stock
    book.owner = req.body.owner
    book.publisher = req.body.publisher
    book.author = req.body.author
    try {
        await book.save()
        res
            .status(201)
            .send({status: true, message: 'success save data.', data: book})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//read
router.get('/book', auth, async (req, res) => {
    try {
        const book = await Book
            .find({})
            .populate('owner')
            .populate('category')
        res
            .status(200)
            .send({status: true, message: 'success read data.', data: book})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//readEdit
router.get('/book/:id', auth, async (req, res) => {
    try {
        const book = await Book
            .findById(req.params.id)
            .populate('owner')
        res
            .status(201)
            .send({status: true, message: 'success read data.', data: book})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//update
router.patch('/book', auth, async (req, res) => {  
    try {
        const book = await Book.findByIdAndUpdate(req.body.id, {
            title: req.body.title,
            description: req.body.description,
            category: req.body.category,
            stock: req.body.stock,
            publisher: req.body.publisher,
            author: req.body.author
        })
        res
            .status(201)
            .send({status: true, message: 'success update data.', data: book})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//delete
router.delete('/book/:id', auth, async (req, res) => {
    try {
        const book = await Book.findByIdAndDelete(req.params.id)
        if (!book) {
            return res
            .status(200)
            .send({status: false, message: 'failed delete data not found!', data: ''})
        }
        res
            .status(200)
            .send({status: true, message: 'success delete data.', data: book})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

module.exports = router