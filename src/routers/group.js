const express = require('express')
const Group = require('../models/group')
const auth = require('../middleware/auth')
const router = new express.Router()

//create
router.post('/group', auth, async (req, res) => {
    const checkname = await Group.findOne(
        {"group_name": req.body.group_name}
    )
    if (checkname) {
        return res
            .status(200)
            .send({status: false, message: 'sorry! group name is already', data: ''})
    }

    const group = new Group()
    group.group_name = req.body.group_name
    group.owner = req.body.owner
    try {
        await group.save()
        res
            .status(201)
            .send({status: true, message: 'success save data.', data: group})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//read
router.get('/group', auth, async (req, res) => {
    try {
        const group = await Group
            .find({})
            .populate('owner')
        res
            .status(200)
            .send({status: true, message: 'success read data.', data: group})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//readEdit
router.get('/group/:id', auth, async (req, res) => {
    try {
        const group = await Group
            .findById(req.params.id)
            .populate('owner')
        res
            .status(201)
            .send({status: true, message: 'success read data.', data: group})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//update
router.patch('/group', auth, async (req, res) => {  
    try {
        const group = await Group.findByIdAndUpdate(req.body.id, {
            group_name: req.body.group_name
        })
        res
            .status(201)
            .send({status: true, message: 'success update data.', data: group})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

//delete
router.delete('/group/:id', auth, async (req, res) => {
    try {
        const group = await Group.findByIdAndDelete(req.params.id)
        if (!group) {
            return res
            .status(200)
            .send({status: false, message: 'failed delete data not found!', data: ''})
        }
        res
            .status(200)
            .send({status: true, message: 'success delete data.', data: group})
    } catch (e) {
        res
            .status(200)
            .send({status: false, message: e.message, data: e})
    }
})

module.exports = router