const mongoose = require('mongoose')

const groupSchema = new mongoose.Schema({
    group_name: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
      },
}, {timestamps: true})

const Group = mongoose.model('Group', groupSchema)

module.exports = Group